//
//  ViewController.swift
//  firebase-ios
//
//  Created by Artem Ustimov on 7/8/18.
//  Copyright © 2018 Artem Ustimov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.displayFCMToken(notification:)),
                                               name: Notification.Name("FCMToken"), object: nil)
    }

    @objc
    func displayFCMToken(notification: NSNotification){
        guard let userInfo = notification.userInfo else {return}
        if let fcmToken = userInfo["token"] as? String {
            self.label.text = fcmToken
        }
    }

    @IBAction func share(_ sender: Any) {
        let activityViewController = UIActivityViewController(activityItems:[self.label.text], applicationActivities:[])
        present(activityViewController, animated: true, completion: nil)
    }
    
}

